const mongoose = require('mongoose');
const { Schema } = mongoose;

const ScoreSchema = new Schema({
    puntaje: { type: Number, required: true},
    date: { type:Date, default: Date.now},
    user: {type: String},
    usna: {type: String}
});
module.exports = mongoose.model('Score', ScoreSchema)