const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Score = require('../models/Scores');
const { isAuthenticated } = require('../helpers/auth');

router.get('/puntajes', isAuthenticated, async (req,res) => {
    const myscores = await Score.find({user: req.user.id}).sort({puntaje: 'desc'});
    const allscores = await Score.find().sort({puntaje: 'desc'});
    console.log(allscores);
    res.render('users/game',{myscores,allscores});
    //res.send('Mis ultimas partidas');
    //res.render('users/signin');
});

router.post('/puntajes/new', isAuthenticated, async (req,res) => {
    
    const {puntaje} = req.body;
    const newScore = new Score({puntaje})
    //console.log(req.body);
        newScore.user = req.user.id;
        newScore.usna = req.user.name;
        await newScore.save();
        req.flash('success_msg','Nuevo Score ');
        res.redirect('/puntajes'); 
});


router.get('/puntajes/add', isAuthenticated, (req,res) => {
    res.render('users/prueba');

    //res.render('users/signin');
});

module.exports = router;