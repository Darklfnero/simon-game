let greenSound = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound1.mp3');
let redSound = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound2.mp3');
let yellowSound = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound3.mp3');
let blueSound = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound4.mp3');
let pinkSound = new Audio('https://taira-komori.jpn.org/sound_os/game01/select09.mp3');
let purpleSound = new Audio('https://taira-komori.jpn.org/sound_os/game01/pickup04.mp3');
let brownSound = new Audio('https://taira-komori.jpn.org/sound_os/game01/blip04.mp3');
let whiteSound = new Audio('https://taira-komori.jpn.org/sound_os/game01/button06.mp3');
let blackSound = new Audio('https://taira-komori.jpn.org/sound_os/game01/button02b.mp3');

const Switch = (props) =>
React.createElement("div", { className: "switch-container" },
React.createElement("span", null, props.label),
React.createElement("label", null,
React.createElement("input", { className: "switch", type: "checkbox" }),
React.createElement("div", { className: "switch-btn", onClick: props.onClick })));




class Controller extends React.Component {
  render() {
    return (
      React.createElement("div", { className: "controller-main-container" },
      React.createElement(Switch, { onClick: this.props.handlePower, label: 'Power' }),
      React.createElement(Switch, { onClick: this.props.handleStrict, label: 'Strict' }),
      React.createElement("a", { className: "button", onClick: this.props.handleStartGame }, "Start"),
      React.createElement("div", { className: "step-counter" }, "Step: ", this.props.step)));


  }}


class Tiles extends React.Component {
  render() {
    return (
      React.createElement("div", { className: `tiles-main-container ${this.props.animate && this.props.wrongAnimation ? 'wrong-tile-animation' : ''}` },
      React.createElement("div", { className: `green-tile ${this.props.animate && this.props.animateTile === 1 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(1);this.props.handleAnimation(1);} }),
      React.createElement("div", { className: `red-tile ${this.props.animate && this.props.animateTile === 2 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(2);this.props.handleAnimation(2);} }),
      React.createElement("div", { className: `yellow-tile ${this.props.animate && this.props.animateTile === 3 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(3);this.props.handleAnimation(3);} }),
      React.createElement("div", { className: `blue-tile ${this.props.animate && this.props.animateTile === 4 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(4);this.props.handleAnimation(4);} }),
      React.createElement("div", { className: `pink-tile ${this.props.animate && this.props.animateTile === 5 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(5);this.props.handleAnimation(5);} }),
      React.createElement("div", { className: `purple-tile ${this.props.animate && this.props.animateTile === 6 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(6);this.props.handleAnimation(6);} }),
      React.createElement("div", { className: `brown-tile ${this.props.animate && this.props.animateTile === 7 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(7);this.props.handleAnimation(7);} }),
      React.createElement("div", { className: `white-tile ${this.props.animate && this.props.animateTile === 8 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(8);this.props.handleAnimation(8);} }),
      React.createElement("div", { className: `black-tile ${this.props.animate && this.props.animateTile === 9 ? 'animation' : ''}`, onClick: () => {this.props.handlePlayer(9);this.props.handleAnimation(9);} })));


  }}


const ModalBox = (props) =>
React.createElement("div", { className: `my-modal ${props.openModal ? 'show-model' : ''}` },
React.createElement("div", { className: "my-modal-content" },
React.createElement("div", { className: "my-modal-body" },
props.children)));





class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      player: false,
      power: false,
      strictMode: false,
      startGame: false,
      replay: false,
      step: 0,
      playerStep: 0,
      simonSequence: [],
      animation: false,
      wrongAnimation: false,
      animateTile: 0,
      modalIsOpen: false };


    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handlePower = this.handlePower.bind(this);
    this.handleStrict = this.handleStrict.bind(this);
    this.handleStartGame = this.handleStartGame.bind(this);
    this.handleAnimation = this.handleAnimation.bind(this);
    this.handlePlayer = this.handlePlayer.bind(this);
    this.playSound = this.playSound.bind(this);
    this.replaySequence = this.replaySequence.bind(this);
    this.randomClick = this.randomClick.bind(this);
  }

  openModal() {
    this.setState({
      modalIsOpen: true });

  }

  closeModal() {
    this.setState({
      player: false,
      startGame: false,
      replay: false,
      step: 0,
      playerStep: 0,
      simonSequence: [],
      animation: false,
      wrongAnimation: false,
      animateTile: 0,
      modalIsOpen: false });


    setTimeout(() => this.handleStartGame(), 1000);
  }

  handlePower() {
    if (this.state.power === false) {
      this.setState({
        power: true });

    } else {
      this.setState({
        player: false,
        power: false,
        strictMode: false,
        startGame: false,
        replay: false,
        step: 0,
        playerStep: 0,
        simonSequence: [],
        animation: false,
        wrongAnimation: false,
        animateTile: 0 });

    }
  }

  handleStrict() {
    if (this.state.strictMode === false) {
      this.setState({
        strictMode: true });

    } else {
      this.setState({
        strictMode: false });

    }
  }

  handleStartGame() {
    if (this.state.power === true && this.state.startGame === false) {
      this.randomClick();
      this.setState({
        startGame: true,
        wrongAnimation: false });

    }
  }

  handleAnimation(tile) {
    if (!this.state.power) return;

    this.setState({
      animation: true,
      animateTile: tile });


    setTimeout(() => {
      this.setState({
        animation: false });

    }, 250);
  }

  handlePlayer(tile) {
    if (!this.state.player) return;
    let currentSequence = this.state.simonSequence.slice();
    let currentPlayerStep = this.state.playerStep;

    this.playSound(tile);

    if (tile !== currentSequence[currentPlayerStep]) {
      // Do some animation to inform the user that he is wrong
      // if we are not in strict mode, replay the sequence to the user
      if (!this.state.strictMode) {
        this.setState({
          player: false,
          animation: true,
          wrongAnimation: true,
          playerStep: 0,
          replay: true });

        this.handleAnimation();
        setTimeout(() => this.replaySequence(), 500);
      } else {
        // we are in strict mode, reset the steps, the game resets and starts again
        this.setState({
          player: false,
          power: true,
          strictMode: true,
          startGame: false,
          replay: false,
          step: 0,
          playerStep: 0,
          simonSequence: [],
          animation: true,
          wrongAnimation: true,
          animateTile: 0 });


        setTimeout(() => this.handleStartGame(), 2000);
      }
    } else if (tile === currentSequence[currentPlayerStep] && currentPlayerStep < currentSequence.length - 1) {
      // we need to promote the step if user click on right tile
      this.setState({
        playerStep: currentPlayerStep + 1,
        wrongAnimation: false });


    } else if (tile === currentSequence[currentPlayerStep] && currentPlayerStep === 19) {
      // If you are here you won :)
      // Show a Modal with replay button
      this.openModal();
    } else if (tile === currentSequence[currentPlayerStep] && currentPlayerStep === currentSequence.length - 1) {
      // if the user accomplished correct sequence replay all the sequence + new step
      this.setState({
        player: false,
        playerStep: 0,
        wrongAnimation: false });

      this.replaySequence();
    }
  }

  playSound(tileNumber) {
    this.handleAnimation(tileNumber);
    switch (tileNumber) {
      case 1:
        greenSound.currentTime = 0;
        greenSound.play();
        break;
      case 2:
        redSound.currentTime = 0;
        redSound.play();
        break;
      case 3:
        yellowSound.currentTime = 0;
        yellowSound.play();
        break;
      case 4:
        blueSound.currentTime = 0;
        blueSound.play();
        break;
      case 5:
        pinkSound.currentTime = 0;
        pinkSound.play();
        break;
      case 6:
        purpleSound.currentTime = 0;
        purpleSound.play();
        break;
      case 7:
        brownSound.currentTime = 0;
        brownSound.play();
        break;
      case 8:
        whiteSound.currentTime = 0;
        whiteSound.play();
        break;
      case 9:
        blackSound.currentTime = 0;
        blackSound.play();
        break;
      default:}

  }

  replaySequence() {
    this.setState({
      wrongAnimation: false });

    let currentSequence = this.state.simonSequence.slice();
    let timeToTrigger = this.state.step + 1;
    currentSequence.forEach((val, i) => {
      setTimeout(() => {
        this.playSound(val);
      }, (i + 1) * 600);
    });

    setTimeout(() => {
      this.randomClick();
    }, timeToTrigger * 600);
  }

  randomClick() {
    let random = Math.floor(Math.random() * 9) + 1;
    let currentStep = this.state.step;
    let currentSequence = this.state.simonSequence.slice();
    let replayStatus = this.state.replay;

    if (replayStatus) {
      this.setState({
        replay: false,
        player: true });

      return;
    }

    currentSequence[currentStep] = random;
    currentStep++;

    this.setState({
      simonSequence: currentSequence,
      step: currentStep,
      player: true });


    this.playSound(random);

  }

  render() {
    return (
      React.createElement("div", { className: "app-main-container" },
      React.createElement(ModalBox, { openModal: this.state.modalIsOpen },
      React.createElement("div", null, "You Won!"),
      React.createElement("a", { className: "button", onClick: this.closeModal }, "Start")),

      React.createElement(Tiles, {
        animateTile: this.state.animateTile,
        animate: this.state.animation,
        wrongAnimation: this.state.wrongAnimation,
        handleAnimation: this.handleAnimation,
        handlePlayer: this.handlePlayer }),
      React.createElement(Controller, {
        power: this.state.power,
        strict: this.state.strictMode,
        step: this.state.step,
        handlePower: this.handlePower,
        handleStrict: this.handleStrict,
        handleStartGame: this.handleStartGame })));


  }}


ReactDOM.render(React.createElement(App, null), document.getElementById('root'));